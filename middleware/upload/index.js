const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./storage/image");
  },

  filename: function (req, file, cb) {
    const nameFile = path.extname(file.originalname);
    cb(null, Date.now() + nameFile);
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    var imageType = ["image/png", "image/jpg", "image/jpeg"];
    if (imageType.includes(file.mimetype)) {
      cb(null, true);
    } else {
      console.log("file phải là png & jpg !");
      cb(null, false);
    }
  },
  limits: {
    fileSize: 1024 * 1024 * 2,
  },
});

module.exports = upload;
