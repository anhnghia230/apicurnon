const jwt = require("jsonwebtoken");

const middlewareAuth = {
  verifyTokenAuth: (req, res, next) => {
    const token = req.headers.authorization;
    if (token) {
      const accessToken = token.split(" ")[1];
      jwt.verify(accessToken, process.env.TOKEN, (error, user) => {
        if (error) {
          res.status(403).json("Token không hợp lệ");
        } else {
          (req.user = user), next();
        }
      });
    } else {
      res.status(401).json("Bạn chưa xác thực");
    }
  },
};

module.exports = middlewareAuth;
