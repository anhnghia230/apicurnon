const vnpayController = require("../../controllers/vnpayController/vnpayController");
const router = require("express").Router();

router.post("/create_payment_url", vnpayController.createPaymentUrl);

module.exports = router;
