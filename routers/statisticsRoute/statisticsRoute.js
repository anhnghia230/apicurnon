const statisticsController = require("../../controllers/Statistics/StatisticsController");
const middlewareAuth = require("../../middleware/middlewareAuth");
const router = require("express").Router();

router.post(
  "/create",
  middlewareAuth.verifyTokenAuth,
  statisticsController.createStatistics
);

router.get("/", statisticsController.getAllStatistics);

router.get("/:id", statisticsController.getAnStatistics);

router.delete(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  statisticsController.deleteStatistics
);

router.put(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  statisticsController.updateStatistics
);

module.exports = router;
