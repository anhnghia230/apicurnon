const brandController = require("../../controllers/brand/brandController");
const middlewareAuth = require("../../middleware/middlewareAuth");
const router = require("express").Router();
const upload = require("../../middleware/upload");

router.post(
  "/create",
  upload.fields([
    { name: "image", maxCount: 1 },
    { name: "banner", maxCount: 1 },
  ]),
  middlewareAuth.verifyTokenAuth,
  brandController.addBrand
);

router.get("/", brandController.getAllBrand);

router.get("/:id", brandController.getAnBrand);

router.delete(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  brandController.deleteBrand
);

router.put(
  "/:id",
  upload.fields([
    { name: "image", maxCount: 1 },
    { name: "banner", maxCount: 1 },
  ]),
  middlewareAuth.verifyTokenAuth,
  brandController.updateBrand
);

module.exports = router;
