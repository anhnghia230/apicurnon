const authController = require("../../controllers/auth/authController");
const middlewareAuth = require("../../middleware/middlewareAuth");
const router = require("express").Router();
const upload = require("../../middleware/upload");

router.post("/register", upload.single("image"), authController.registerUser);

router.post("/login", authController.login);

router.put(
  "/update/:id",
  middlewareAuth.verifyTokenAuth,
  upload.single("image"),
  authController.updateAdmin
);

router.put(
  "/updatePassword/:id",
  middlewareAuth.verifyTokenAuth,
  authController.updatePassword
);

router.delete("/:id", authController.delete);

router.get("/user/:id", authController.getUserAdmin);

module.exports = router;
