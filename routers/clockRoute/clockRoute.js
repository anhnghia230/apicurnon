const clockController = require("../../controllers/clock/clockController");
const middlewareAuth = require("../../middleware/middlewareAuth");
const router = require("express").Router();
const upload = require("../../middleware/upload");

router.post(
  "/create",
  middlewareAuth.verifyTokenAuth,
  upload.single("image"),
  clockController.addClock
);

router.get("/", clockController.getAll);

router.get("/:id", clockController.getAn);

router.delete(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  clockController.deleteClock
);

router.put(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  upload.single("image"),
  clockController.updateClock
);

module.exports = router;
