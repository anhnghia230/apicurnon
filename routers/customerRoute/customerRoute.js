const customerController = require("../../controllers/customer/customerController");
const middlewareAuth = require("../../middleware/middlewareAuth");
const router = require("express").Router();

router.get("/", customerController.getAllCustomer);

router.get("/:id", customerController.getAnCustomer);

router.post("/create", customerController.createCustomer);

router.delete(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  customerController.deleteCustomer
);

router.put(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  customerController.updateCustomer
);

module.exports = router;
