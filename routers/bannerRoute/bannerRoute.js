const bannerController = require("../../controllers/banner/bannerController");
const middlewareAuth = require("../../middleware/middlewareAuth");
const router = require("express").Router();
const upload = require("../../middleware/upload");

router.post(
  "/create",
  middlewareAuth.verifyTokenAuth,
  upload.single("image"),
  bannerController.addBanner
);

router.get("/", bannerController.getAllBanner);

router.get("/:id", bannerController.getAnBanner);

router.delete(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  bannerController.deleteBanner
);

router.put(
  "/:id",
  middlewareAuth.verifyTokenAuth,
  upload.single("image"),
  bannerController.updateBanner
);

module.exports = router;
