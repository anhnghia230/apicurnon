const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
var bodyParser = require("body-parser");
const morgan = require("morgan");
const dotenv = require("dotenv");

//route
const authRoute = require("./routers/authRoute/authRoute");
const brandRoute = require("./routers/brandRoute/brandRoute");
const bannerRoute = require("./routers/bannerRoute/bannerRoute");
const clockRoute = require("./routers/clockRoute/clockRoute");
const customerRoute = require("./routers/customerRoute/customerRoute");
const vnpayRoute = require("./routers/vnpayRoute/vnpayRoute");
const statisticsRoute = require("./routers/statisticsRoute/statisticsRoute");

dotenv.config();

//connect database

mongoose.connect(process.env.MONGODB_URL, () => {
  console.log("connected to mongooDB");
});

app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());
app.use(morgan("common"));

app.use("/api/v1/admin", authRoute);
app.use("/api/v1/brand", brandRoute);
app.use("/api/v1/banner", bannerRoute);
app.use("/api/v1/clock", clockRoute);
app.use("/api/v1/customer", customerRoute);
app.use("/api/v1/vnpay", vnpayRoute);
app.use("/api/v1/statistics", statisticsRoute);
app.use("/storage/image", express.static("storage/image"));

app.listen(8000, () => {
  console.log("server is runging...");
});
