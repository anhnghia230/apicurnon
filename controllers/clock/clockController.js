const { Clock, Brand } = require("../../module");

const clockController = {
  addClock: async (req, res) => {
    try {
      const clock = new Clock(req.body);
      if (req.file) {
        clock.image = req.file.path;
      }
      const newClock = await clock.save();
      if (req.body.brand) {
        const brand = Brand.findById(req.body.brand);
        await brand.updateOne({ $push: { clocks: newClock._id } });
      }

      res.status(200).json(newClock);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAll: async (req, res) => {
    try {
      const clockAll = await Clock.find().populate("brand");
      res.status(200).json(clockAll);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAn: async (req, res) => {
    try {
      const clock = await Clock.findById(req.params.id);
      res.status(200).json(clock);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  deleteClock: async (req, res) => {
    try {
      await Brand.updateMany(
        { clocks: req.params.id },
        { $pull: { clocks: req.params.id } }
      );
      await Clock.findByIdAndDelete(req.params.id);
      res.status(200).json("Xóa sản phẩm thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
  updateClock: async (req, res) => {
    try {
      const body = req.body;
      const clock = await Clock.findById(req.params.id);
      const image = req.file ? req.file.path : clock.image;
      await clock.updateOne({
        $set: { ...body, image: image },
      });
      res.status(200).json("Cập nhập sản phẩm thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
};

module.exports = clockController;
