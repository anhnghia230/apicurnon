const { Customer } = require("../../module");

const customerController = {
  createCustomer: async (req, res) => {
    try {
      const createCustomer = new Customer(req.body);
      createCustomer.orderConfirmation = false;
      createCustomer.successfulDelivery = false;
      createCustomer.statusPayment = "Chưa thanh toán";
      const newCustomer = await createCustomer.save();
      res.status(200).json(newCustomer);
    } catch (error) {
      res.status(500).json("Đặt hàng không thành công");
    }
  },
  getAllCustomer: async (req, res) => {
    try {
      const getAllCustomer = await Customer.find().populate("clocks._id");
      res.status(200).json(getAllCustomer);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAnCustomer: async (req, res) => {
    try {
      const getAnCustomer = await Customer.findById(req.params.id).populate(
        "clocks._id"
      );
      res.status(200).json(getAnCustomer);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  deleteCustomer: async (req, res) => {
    try {
      await Customer.findByIdAndDelete(req.params.id);
      res.status(200).json("Xóa khách hàng thành công");
    } catch (error) {
      res.status(500).json("Xóa khách hàng không thành công");
    }
  },
  updateCustomer: async (req, res) => {
    try {
      const customer = await Customer.findById(req.params.id);
      await customer.updateOne({ $set: req.body });
      res.status(200).json("cập nhập thông tin đơn hàng thành công");
    } catch (error) {
      res.status(500).json("Cập nhập thông tin đơn hàng không thành công.");
    }
  },
};

module.exports = customerController;
