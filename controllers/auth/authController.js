const { User } = require("../../module");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const authController = {
  registerUser: async (req, res) => {
    try {
      const salt = await bcrypt.genSalt(10);
      const hashed = await bcrypt.hash(req.body.password, salt);

      // tạo admin
      const newUser = new User({
        userName: req.body.userName,
        name: req.body.userName,
        email: req.body.email,
        image: req.file ? req.file.path : "",
        password: hashed,
      });
      const user = await newUser.save();
      res.status(200).json(user);
    } catch (error) {
      res.status(500).json(error);
    }
  },

  login: async (req, res) => {
    try {
      const user = await User.findOne({ userName: req.body.userName });
      if (!user) {
        res.status(404).json("Tài khoản hoặc mật khẩu không chính xác");
      }
      const Validpassword = await bcrypt.compare(
        req.body.password,
        user.password
      );

      if (!Validpassword) {
        res.status(404).json("Tài khoản hoặc mật khẩu không chính xác!");
      }
      if (user && Validpassword) {
        const token = jwt.sign(
          {
            id: user.id,
          },
          process.env.TOKEN
        );
        const { password, ...orther } = user._doc;
        res.status(200).json({ ...orther, token });
      }
    } catch (error) {
      res.status(500).json(error);
    }
  },

  getUserAdmin: async (req, res) => {
    try {
      const user = await User.findById(req.params.id);
      const { password, ...orther } = user._doc;
      res.status(200).json({ ...orther });
    } catch (error) {
      res.status(500).json(error);
    }
  },

  updateAdmin: async (req, res) => {
    try {
      const body = req.body;
      const user = await User.findById(req.params.id);
      const image = req.file ? req.file.path : user.image;
      await user.updateOne({
        $set: { ...body, image: image },
      });
      res.status(200).json("cập nhập thông tin thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },

  updatePassword: async (req, res) => {
    try {
      const body = req.body;
      const salt = await bcrypt.genSalt(10);
      const hashed = await bcrypt.hash(req.body.newPassword, salt);

      const user = await User.findById(req.params.id);

      const Validpassword = await bcrypt.compare(
        req.body.password,
        user.password
      );
      if (Validpassword) {
        await user.updateOne({
          $set: { ...body, password: hashed },
        });

        res.status(200).json("Thay đổi mật khảu thành công");
      } else {
        res.status(404).json("Mật khẩu không đúng");
      }
    } catch (error) {
      res.status(500).json(error);
    }
  },
  delete: async (req, res) => {
    try {
      await User.findByIdAndDelete(req.params.id);
      res.status(200).json("Xóa tài khoản admin thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
};

module.exports = authController;
