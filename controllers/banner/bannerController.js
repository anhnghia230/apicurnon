const { Banner } = require("../../module");

const bannerController = {
  addBanner: async (req, res) => {
    try {
      const banner = new Banner(req.body);
      if (req.file) {
        banner.image = req.file.path;
      }
      const newBanner = await banner.save();
      res.status(200).json(newBanner);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAllBanner: async (req, res) => {
    try {
      const banner = await Banner.find();
      res.status(200).json(banner);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAnBanner: async (req, res) => {
    try {
      const banner = await Banner.findById(req.params.id);
      res.status(200).json(banner);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  deleteBanner: async (req, res) => {
    try {
      await Banner.findByIdAndDelete(req.params.id);
      res.status(200).json("Xóa Banner thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
  updateBanner: async (req, res) => {
    try {
      const body = req.body;
      const banner = await Banner.findById(req.params.id);
      const image = req.file ? req.file.path : banner.image;
      await banner.updateOne({
        $set: { ...body, image: image },
      });

      res.status(200).json("cập nhập banner thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
};

module.exports = bannerController;
