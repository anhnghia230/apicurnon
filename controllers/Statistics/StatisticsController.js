const { Statistics } = require("../../module");

const statisticsController = {
  createStatistics: async (req, res) => {
    try {
      const createStatistics = new Statistics(req.body);
      const newStatistics = await createStatistics.save();
      res.status(200).json(newStatistics);
    } catch (error) {
      res.status(500).json("Thêm thông tin thống kê thành công");
    }
  },
  getAllStatistics: async (req, res) => {
    try {
      const getAllStatistics = await Statistics.find();
      res.status(200).json(getAllStatistics);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAnStatistics: async (req, res) => {
    try {
      const getAnStatistics = await Statistics.findById(req.params.id);
      res.status(200).json(getAnStatistics);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  deleteStatistics: async (req, res) => {
    try {
      await Statistics.findByIdAndDelete(req.params.id);
      res.status(200).json("Xóa thông tin thống kê thành công");
    } catch (error) {
      res.status(500).json("Xóa thông tin thống kê không thành công");
    }
  },
  updateStatistics: async (req, res) => {
    try {
      const customer = await Statistics.findById(req.params.id);
      await customer.updateOne({ $set: req.body });
      res.status(200).json("cập nhập thông tin thống kê thành công");
    } catch (error) {
      res.status(500).json("Cập nhập thông tin thống kê không thành công.");
    }
  },
};

module.exports = statisticsController;
