const { Brand, Clock } = require("../../module");

const brandController = {
  addBrand: async (req, res) => {
    try {
      const brand = new Brand(req.body);
      if (req.files) {
        brand.image = req.files["image"][0].path;
        brand.banner = req.files["banner"][0].path;
      }
      const newBrand = await brand.save();
      res.status(200).json(newBrand);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAllBrand: async (req, res) => {
    try {
      const getBrand = await Brand.find().populate("clocks");
      res.status(200).json(getBrand);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getAnBrand: async (req, res) => {
    try {
      const getBrand = await Brand.findById(req.params.id).populate("clocks");
      res.status(200).json(getBrand);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  deleteBrand: async (req, res) => {
    try {
      await Clock.deleteMany({ brand: req.params.id });
      await Brand.findByIdAndDelete(req.params.id);
      res.status(200).json("Xóa thương hiệu thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
  updateBrand: async (req, res) => {
    try {
      const body = req.body;
      const brand = await Brand.findById(req.params.id);
      const image =
        req.files && req.files["image"]
          ? req.files["image"][0].path
          : brand.image;
      const banner =
        req.files && req.files["banner"]
          ? req.files["banner"][0].path
          : brand.banner;
      await brand.updateOne({
        $set: { ...body, image: image, banner: banner },
      });
      res.status(200).json("Cập nhập thương hiệu thành công");
    } catch (error) {
      res.status(500).json(error);
    }
  },
};

module.exports = brandController;
