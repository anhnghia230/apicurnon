const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      require: true,
      minlength: 6,
      maxlenght: 50,
      unique: true,
    },
    name: {
      type: String,
      require: true,
      minlength: 6,
      maxlenght: 50,
    },
    email: {
      type: String,
      require: true,
      minlength: 12,
      maxlenght: 50,
      unique: true,
    },
    image: {
      type: String,
    },
    password: {
      type: String,
      require: true,
      minlength: 6,
    },
  },
  { timestamps: true }
);

const brandSchema = new mongoose.Schema(
  {
    image: {
      type: String,
      require: true,
    },
    name: {
      type: String,
      unique: true,
      require: true,
    },
    banner: {
      type: String,
      require: true,
    },
    desc: {
      type: String,
      require: true,
    },
    genres: {
      type: String,
      require: true,
    },
    clocks: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Clock",
      },
    ],
  },
  { timestamps: true }
);

const clockSchema = new mongoose.Schema(
  {
    image: {
      type: String,
      require: true,
    },
    name: {
      type: String,
      require: true,
    },
    brand: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Brand",
      require: true,
    },
    price: {
      type: Number,
      require: true,
    },
    sizeFace: {
      type: String,
      require: true,
    },
    thickness: {
      type: String,
      require: true,
    },
    colorFace: {
      type: String,
      require: true,
    },
    genres: {
      type: String,
      require: true,
    },
    sizeWire: {
      type: String,
      require: true,
    },
    Waterproof: {
      type: String,
      require: true,
    },
    faceGlasses: {
      type: String,
      require: true,
    },
    wireMaterial: {
      type: String,
      require: true,
    },
    genresClock: {
      type: String,
      require: true,
    },
    sell: {
      type: String,
    },
    desc: {
      type: String,
      require: true,
    },
    quantity: {
      type: Number,
      require: true,
    },
  },
  { timestamps: true }
);

const bannerSchema = new mongoose.Schema(
  {
    image: {
      type: String,
      require: true,
    },
    preTitle: {
      type: String,
      require: true,
    },
    title: {
      type: String,
      require: true,
    },
    desc: {
      type: String,
      require: true,
    },
  },
  { timestamps: true }
);

const customerSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      require: true,
    },
    name: {
      type: String,
      require: true,
    },
    phone: {
      type: String,
      require: true,
    },
    address: {
      type: String,
      require: true,
    },
    province: {
      type: String,
      require: true,
    },
    district: {
      type: String,
      require: true,
    },
    clocks: [
      {
        _id: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Clock",
          require: true,
        },
        count: {
          type: Number,
          require: true,
        },
      },
    ],
    ward: {
      type: String,
      require: true,
    },
    orderConfirmation: {
      type: Boolean,
      default: false,
    },
    successfulDelivery: {
      type: Boolean,
      default: false,
    },
    statusPayment: {
      type: String,
      require: true,
    },
    desc: {
      type: String,
    },
    totalPrice: {
      type: Number,
      require: true,
    },
  },
  { timestamps: true }
);
const statisticsSchema = new mongoose.Schema(
  {
    dayDate: {
      type: String,
      require: true,
    },
    quantity: {
      type: Number,
      require: true,
    },
    totalPrice: {
      type: Number,
      require: true,
    },
  },
  { timestamps: true }
);
let User = mongoose.model("User", userSchema);
let Brand = mongoose.model("Brand", brandSchema);
let Banner = mongoose.model("Banner", bannerSchema);
let Clock = mongoose.model("Clock", clockSchema);
let Customer = mongoose.model("Customer", customerSchema);
let Statistics = mongoose.model("Statistics", statisticsSchema);
module.exports = {
  User,
  Brand,
  Banner,
  Clock,
  Customer,
  Statistics,
};
